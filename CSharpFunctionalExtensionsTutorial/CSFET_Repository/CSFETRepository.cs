﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions.Tutorial.Repository.Model;
using Newtonsoft.Json.Linq;

namespace CSharpFunctionalExtensions.Tutorial.Repository
{
    public class CSFETRepository : ICSFETRepository
    {
        private void LoadFromJson(string json)
        {
            throw new NotImplementedException();
        }

        public CSFETRepository(string json)
        {
            LoadFromJson(json);
        }

        public async Task<Result<PilotDBO, string>> CreatePilot(string firstName, string lastName, string title, int age)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<PilotDBO, string>> ReadPilot(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<PilotDBO, string>> ReadPilot(string firstName, string lastName)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<IEnumerable<PilotDBO>, string>> ReadPilots()
        {
            throw new NotImplementedException();
        }

        public async Task<Result<PilotDBO, string>> UpdatePilot(Guid id, string firstName, string lastName, string title, int? age)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<Guid, string>> DeletePilot(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaDBO, string>> CreateEva(string unit, string type, string color)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaDBO, string>> ReadEva(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaDBO, string>> ReadEva(string unit)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<IEnumerable<EvaDBO>, string>> ReadEvas()
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaDBO, string>> UpdateEva(Guid id, string unit, string type, string color)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<Guid, string>> DeleteEva(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaPilotDBO, string>> CreateEvaPilot(Guid evaId, Guid pilotId, double sync)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaPilotDBO, string>> CreateEvaPilot(string unit, string firstName, string lastName, double sync)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaPilotDBO, string>> ReadEvaPilot(Guid evaId, Guid pilotId)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<IEnumerable<EvaPilotDBO>, string>> ReadEvaPilots()
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaPilotDBO, string>> UpdateEvaPilot(Guid evaId, Guid pilotId, double sync)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<(Guid, Guid), string>> DeleteEvaPilot(Guid evaId, Guid pilotId)
        {
            throw new NotImplementedException();
        }

        public async Task<Result<EvaWithPilotDBO, string>> ReadEvaWithPilot(string unit)
        {
            throw new NotImplementedException();
        }
    }
}
