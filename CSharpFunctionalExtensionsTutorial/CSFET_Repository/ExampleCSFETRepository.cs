﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions.Tutorial.Repository.Model;
using Newtonsoft.Json.Linq;

namespace CSharpFunctionalExtensions.Tutorial.Repository
{
    public class ExampleCSFETRepository : ICSFETRepository
    {
        private List<PilotDBO> _pilots;
        private List<EvaDBO> _evas;
        private Dictionary<Guid, (Guid, double)?> _evaPilots;

        private void LoadFromJson(string json)
        {
            var jObject = JObject.Parse(json);

            _pilots = jObject["pilots"].Select(x => x.ToObject<PilotDBO>()).ToList();

            _evas = jObject["evas"].Select(x => x.ToObject<EvaDBO>()).ToList();

            _evaPilots = _evas.ToDictionary(
                new Func<EvaDBO, Guid>(x => x.Id),
                new Func<EvaDBO, (Guid, double)?>(x => null),
                EqualityComparer<Guid>.Default);
        }

        public ExampleCSFETRepository(string json)
        {
            LoadFromJson(json);
        }

        public async Task<Result<PilotDBO, string>> CreatePilot(string firstName, string lastName, string title, int age)
        {
            var record = new PilotDBO
            {
                FirstName = firstName,
                LastName = lastName,
                Title = title,
                Age = age
            };

            _pilots.Add(record);

            return await Task.FromResult(Result.Success<PilotDBO, string>(record));
        }

        public async Task<Result<PilotDBO, string>> ReadPilot(Guid id)
        {
            var record = _pilots.SingleOrDefault(x => x.Id == id);

            return await Task.FromResult(record != null
                ? Result.Success<PilotDBO, string>(record)
                : Result.Failure<PilotDBO, string>($"Pilot with id {id} not found"));
        }

        public async Task<Result<PilotDBO, string>> ReadPilot(string firstName, string lastName)
        {
            var record = _pilots.SingleOrDefault(x => x.FirstName == firstName && x.LastName == lastName);

            return await Task.FromResult(record != null
                ? Result.Success<PilotDBO, string>(record)
                : Result.Failure<PilotDBO, string>($"Pilot with name {firstName} {lastName} not found"));
        }

        public async Task<Result<IEnumerable<PilotDBO>, string>> ReadPilots()
        {
            return await Task.FromResult(Result.Success<IEnumerable<PilotDBO>, string>(_pilots));
        }

        public async Task<Result<PilotDBO, string>> UpdatePilot(Guid id, string firstName, string lastName, string title, int? age)
        {
            return await ReadPilot(id)
                .Map(record =>
                {
                    record.FirstName = string.IsNullOrWhiteSpace(firstName) ? record.FirstName : firstName;
                    record.LastName = string.IsNullOrWhiteSpace(lastName) ? record.LastName : lastName;
                    record.Title = string.IsNullOrWhiteSpace(title) ? record.Title : title;
                    record.Age = !age.HasValue ? record.Age : age.Value;

                    return record;
                });
        }

        public async Task<Result<Guid, string>> DeletePilot(Guid id)
        {
            return await ReadPilot(id)
                .TapIf(_evaPilots.Any(x => x.Value.HasValue && x.Value.Value.Item1 == id),
                async record =>
                {
                    var record2 = _evaPilots.Single(x => x.Value.HasValue && x.Value.Value.Item1 == id);
                    await DeleteEvaPilot(record2.Key, record2.Value.Value.Item1);
                })
                .Tap(_ =>
                {
                    _pilots = _pilots.Where(x => x.Id != id).ToList();
                })
                .Map(pilot => pilot.Id);
        }

        public async Task<Result<EvaDBO, string>> CreateEva(string unit, string type, string color)
        {
            if(_evas.Any(x => x.Unit == unit))
            {
                return await Task.FromResult(Result.Failure<EvaDBO, string>($"Unit {unit} already in production"));
            }

            var record = new EvaDBO
            {
                Unit = unit,
                Type = type,
                Color = color
            };

            _evas.Add(record);

            _evaPilots.Add(record.Id, null);

            return await Task.FromResult(Result.Success<EvaDBO, string>(record));
        }

        public async Task<Result<EvaDBO, string>> ReadEva(Guid id)
        {
            var record = _evas.SingleOrDefault(x => x.Id == id);

            return await Task.FromResult(record != null
                ? Result.Success<EvaDBO, string>(record)
                : Result.Failure<EvaDBO, string>($"Eva with id {id} not found"));
        }

        public async Task<Result<EvaDBO, string>> ReadEva(string unit)
        {
            var record = _evas.SingleOrDefault(x => x.Unit == unit);

            return await Task.FromResult(record != null
                ? Result.Success<EvaDBO, string>(record)
                : Result.Failure<EvaDBO, string>($"Unit {unit} not in production"));
        }

        public async Task<Result<IEnumerable<EvaDBO>, string>> ReadEvas()
        {
            return await Task.FromResult(Result.Success<IEnumerable<EvaDBO>, string>(_evas));
        }

        public async Task<Result<EvaDBO, string>> UpdateEva(Guid id, string unit, string type, string color)
        {
            return await ReadEva(id)
                .Map(record =>
                {
                    record.Unit = string.IsNullOrWhiteSpace(unit) ? record.Unit : unit;
                    record.Type = string.IsNullOrWhiteSpace(type) ? record.Type : type;
                    record.Color = string.IsNullOrWhiteSpace(color) ? record.Color : color;

                    return record;
                });
        }

        public async Task<Result<Guid, string>> DeleteEva(Guid id)
        {
            return await ReadEva(id)
                .Tap(_ =>
                {
                    _evas = _evas.Where(x => x.Id != id).ToList();
                    _evaPilots.Remove(id);
                })
                .Map(eva => eva.Id);
        }

        public async Task<Result<EvaPilotDBO, string>> CreateEvaPilot(Guid evaId, Guid pilotId, double sync)
        {
            return await CreateEvaPilot(await ReadEva(evaId), await ReadPilot(pilotId), sync);
        }

        public async Task<Result<EvaPilotDBO, string>> CreateEvaPilot(string unit, string firstName, string lastName, double sync)
        {
            return await CreateEvaPilot(await ReadEva(unit), await ReadPilot(firstName, lastName), sync);
        }

        public async Task<Result<EvaPilotDBO, string>> ReadEvaPilot(Guid evaId, Guid pilotId)
        {
            return await ReadEva(evaId)
                .Map(_ => _evaPilots[evaId])
                .Ensure(record => record.HasValue && record.Value.Item1 == pilotId, $"Pilot with id {pilotId} not assigned to Eva with id {evaId}")
                .Map(record => new EvaPilotDBO
                {
                    EvaId = evaId,
                    PilotId = record.Value.Item1,
                    SynchronizationRate = record.Value.Item2
                });
        }

        public async Task<Result<IEnumerable<EvaPilotDBO>, string>> ReadEvaPilots()
        {
            var results = await Task.WhenAll(_evaPilots.Where(x => x.Value.HasValue).Select(async x => await ReadEvaPilot(x.Key, x.Value.Value.Item1)).ToArray());

            return results.Any(x => x.IsFailure)
                ? Result.Failure<IEnumerable<EvaPilotDBO>, string>("Error reading Eva and Pilot assignment records")
                : Result.Success<IEnumerable<EvaPilotDBO>, string>(results.Select(x => x.Value));
        }

        public async Task<Result<EvaPilotDBO, string>> UpdateEvaPilot(Guid evaId, Guid pilotId, double sync)
        {
            return await ReadEvaPilot(evaId, pilotId)
                .Tap(_ =>
                {
                    _evaPilots[evaId] = (pilotId, sync);
                })
                .Bind(async _ => await ReadEvaPilot(evaId, pilotId));
        }

        public async Task<Result<(Guid, Guid), string>> DeleteEvaPilot(Guid evaId, Guid pilotId)
        {
            return await ReadEvaPilot(evaId, pilotId)
                .Map(_ =>
                {
                    _evaPilots[evaId] = null;
                    return (evaId, pilotId);
                });
        }

        public async Task<Result<EvaWithPilotDBO, string>> ReadEvaWithPilot(string unit)
        {
            return await ReadEva(unit)
                .Bind(async eva =>
                {
                    var result = _evaPilots[eva.Id].HasValue
                        ? await ReadPilot(_evaPilots[eva.Id].Value.Item1)
                            .Map<PilotDBO, (PilotDBO, double?), string>(pilot => (pilot, _evaPilots[eva.Id].Value.Item2))
                        : Result.Success<(PilotDBO, double?), string>((null, null));

                    return result.Map(pilotAndSync => new EvaWithPilotDBO
                    {
                        Eva = eva,
                        Pilot = pilotAndSync.Item1,
                        SynchronizationRate = pilotAndSync.Item2
                    });
                });
        }

        private async Task<Result<EvaPilotDBO, string>> CreateEvaPilot(Result<EvaDBO, string> evaResult, Result<PilotDBO, string> pilotResult, double sync)
        {
            return await Task.FromResult(evaResult
                .Bind(eva => pilotResult
                    .Bind(pilot => Result.Success<(EvaDBO, PilotDBO), string>((eva, pilot))))
                .Check(evaAndPilot =>
                {
                    if (_evaPilots[evaAndPilot.Item1.Id].HasValue)
                    {
                        return Result.Failure<(EvaDBO, PilotDBO), string>($"Eva with id {evaResult.Value.Id} already has a Pilot");
                    }

                    if (_evaPilots.Any(x => x.Value.HasValue && x.Value.Value.Item1 == evaAndPilot.Item2.Id))
                    {
                        return Result.Failure<(EvaDBO, PilotDBO), string>($"Pilot with id {pilotResult.Value.Id} is already assigned to an Eva");
                    }

                    return Result.Success<(EvaDBO, PilotDBO), string>(evaAndPilot);
                })
                .Map(evaAndPilot =>
                {
                    _evaPilots[evaAndPilot.Item1.Id] = (evaAndPilot.Item2.Id, sync);

                    var record = new EvaPilotDBO
                    {
                        EvaId = evaAndPilot.Item1.Id,
                        PilotId = evaAndPilot.Item2.Id,
                        SynchronizationRate = sync
                    };

                    return record;
                }));
        }
    }
}
