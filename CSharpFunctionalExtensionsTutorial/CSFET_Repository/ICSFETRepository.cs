﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions.Tutorial.Repository.Model;

namespace CSharpFunctionalExtensions.Tutorial.Repository
{
    public interface ICSFETRepository
    {
        Task<Result<PilotDBO, string>> CreatePilot(string firstName, string lastName, string title, int age);
        Task<Result<PilotDBO, string>> ReadPilot(Guid id);
        Task<Result<PilotDBO, string>> ReadPilot(string firstName, string lastName);
        Task<Result<IEnumerable<PilotDBO>, string>> ReadPilots();
        Task<Result<PilotDBO, string>> UpdatePilot(Guid id, string firstName, string lastName, string title, int? age);
        Task<Result<Guid, string>> DeletePilot(Guid id);
        Task<Result<EvaDBO, string>> CreateEva(string unit, string type, string color);
        Task<Result<EvaDBO, string>> ReadEva(Guid id);
        Task<Result<EvaDBO, string>> ReadEva(string unit);
        Task<Result<IEnumerable<EvaDBO>, string>> ReadEvas();
        Task<Result<EvaDBO, string>> UpdateEva(Guid id, string unit, string type, string color);
        Task<Result<Guid, string>> DeleteEva(Guid id);
        Task<Result<EvaPilotDBO, string>> CreateEvaPilot(Guid evaId, Guid pilotId, double sync);
        Task<Result<EvaPilotDBO, string>> CreateEvaPilot(string unit, string firstName, string lastName, double sync);
        Task<Result<EvaPilotDBO, string>> ReadEvaPilot(Guid evaId, Guid pilotId);
        Task<Result<IEnumerable<EvaPilotDBO>, string>> ReadEvaPilots();
        Task<Result<EvaPilotDBO, string>> UpdateEvaPilot(Guid evaId, Guid pilotId, double sync);
        Task<Result<(Guid, Guid), string>> DeleteEvaPilot(Guid evaId, Guid pilotId);
        Task<Result<EvaWithPilotDBO, string>> ReadEvaWithPilot(string unit);
    }
}
