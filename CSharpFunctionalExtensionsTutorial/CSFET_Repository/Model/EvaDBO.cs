﻿using System;

namespace CSharpFunctionalExtensions.Tutorial.Repository.Model
{
    public class EvaDBO
    {
        public Guid Id { get; private set; }
        public string Unit { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }

        public EvaDBO()
        {
            Id = Guid.NewGuid();
        }
    }
}
