﻿using System;

namespace CSharpFunctionalExtensions.Tutorial.Repository.Model
{
    public class EvaPilotDBO
    {
        public Guid EvaId { get; set; }
        public Guid PilotId { get; set; }
        public double SynchronizationRate { get; set; }
    }
}
