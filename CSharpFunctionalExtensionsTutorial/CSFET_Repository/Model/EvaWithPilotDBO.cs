﻿using System;

namespace CSharpFunctionalExtensions.Tutorial.Repository.Model
{
    public class EvaWithPilotDBO
    {
        public EvaDBO Eva { get; set; }
        public PilotDBO Pilot { get; set; }
        public double? SynchronizationRate { get; set; }
    }
}
