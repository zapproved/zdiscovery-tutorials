﻿using System;

namespace CSharpFunctionalExtensions.Tutorial.Repository.Model
{
    public class PilotDBO
    {
        public Guid Id { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public int Age { get; set; }

        public PilotDBO()
        {
            Id = Guid.NewGuid();
        }
    }
}
