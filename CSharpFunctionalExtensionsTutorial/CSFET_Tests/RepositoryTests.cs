﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using CSharpFunctionalExtensions.Tutorial.Repository;
using CSharpFunctionalExtensions.Tutorial.Repository.Model;
using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.CSharpFunctionalExtensions;
using NUnit.Framework;

namespace CSFET_Tests
{
    [TestFixture]
    [NonParallelizable]
    public class RepositoryTests
    {
        private ICSFETRepository _repository;

        private string ReadJson(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        [OneTimeSetUp]
        public void Init()
        {
            var json = ReadJson("CSFET_Tests.Assets.json_repository.json");
            //_repository = new ExampleCSFETRepository(json);
            _repository = new CSFETRepository(json);
        }

        [Test, Order(1)]
        public async Task WhyDidYouSendForMe()
        {
            var result = await _repository.ReadPilot("Shinji", "Ikari");

            result.IsSuccess.Should().BeTrue();
            result.Value.Should().NotBeNull();
            result.Value.FirstName.Should().Be("Shinji");
            result.Value.LastName.Should().Be("Ikari");
            result.Value.Age.Should().Be(14);
        }

        [Test, Order(2)]
        public async Task IMustNotRunAway()
        {
            var sync = 41.3;

            var result1 = await _repository.CreateEvaPilot("01", "Shinji", "Ikari", sync);

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("01");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("01");
                result2.Value.Eva.Type.Should().Be("Test Type");
                result2.Value.Eva.Color.Should().Be("Purple");
                result2.Value.Pilot.FirstName.Should().Be("Shinji");
                result2.Value.Pilot.LastName.Should().Be("Ikari");
                result2.Value.Pilot.Title.Should().Be("Third Child");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(3)]
        public async Task IAmBondedToIt()
        {
            var sync = 60;
            var color = "Blue";

            var result1 = await _repository.CreateEvaPilot("00", "Rei", "Ayanami", sync)
                .Check(async evaPilot => await _repository.UpdateEva(evaPilot.EvaId, null, null, color));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("00");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("00");
                result2.Value.Eva.Type.Should().Be("Prototype");
                result2.Value.Eva.Color.Should().Be(color);
                result2.Value.Pilot.FirstName.Should().Be("Rei");
                result2.Value.Pilot.LastName.Should().Be("Ayanami");
                result2.Value.Pilot.Title.Should().Be("First Child");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(4)]
        public async Task AsukaStrikes()
        {
            var sync = 65;

            var result1 = await _repository.CreateEvaPilot("02", "Asuka", "Soryu", sync)
                .Check(async _ => await _repository.ReadPilot("Shinji", "Ikari")
                    .Bind(async pilot => await _repository.UpdatePilot(pilot.Id, null, null, "Shinji You Idiot!", null)))
                .Bind(async evaPilot => await _repository.UpdateEvaPilot(evaPilot.EvaId, evaPilot.PilotId, sync / 2));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("02");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("02");
                result2.Value.Eva.Type.Should().Be("Combat Type");
                result2.Value.Eva.Color.Should().Be("Red");
                result2.Value.Pilot.FirstName.Should().Be("Asuka");
                result2.Value.Pilot.LastName.Should().Be("Soryu");
                result2.Value.Pilot.Title.Should().Be("Second Child");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync / 2);
            }

            var result3 = await _repository.ReadPilot("Shinji", "Ikari");

            using (new AssertionScope())
            {
                result3.Value.FirstName.Should().Be("Shinji");
                result3.Value.LastName.Should().Be("Ikari");
                result3.Value.Title.Should().Be("Shinji You Idiot!");
                result3.Value.Age.Should().Be(14);
            }
        }

        [Test, Order(5)]
        public async Task Matarael()
        {
            var result = await _repository.ReadEvaPilots()
                .Bind(async evaPilots =>
                {
                    var results = await Task.WhenAll(evaPilots.Select(async x => await _repository.ReadEva(x.EvaId)));

                    return results.Any(x => x.IsFailure)
                        ? Result.Failure<IEnumerable<EvaDBO>, string>("Error reading Eva records")
                        : Result.Success<IEnumerable<EvaDBO>, string>(results.Select(x => x.Value));
                })
                .Bind(async evas =>
                {
                    var results = await Task.WhenAll(evas.Select(async x => await _repository.ReadEvaWithPilot(x.Unit)));

                    return results.Any(x => x.IsFailure)
                        ? Result.Failure<IEnumerable<EvaWithPilotDBO>, string>("Error reading Eva and Pilot assignment records")
                        : Result.Success<IEnumerable<EvaWithPilotDBO>, string>(results.Select(x => x.Value));
                });

            using (new AssertionScope())
            {
                result.IsSuccess.Should().BeTrue();
                result.Value.Should().NotBeNull();
                Assert.That(result.Value, Has.Exactly(3).Items);
                CollectionAssert.AreEquivalent(result.Value.Select(x => x.Eva.Unit), new[] { "00", "01", "02" });
                CollectionAssert.AreEquivalent(
                    result.Value.Select(x => $"{x.Pilot.FirstName} {x.Pilot.LastName}"),
                    new[] { "Rei Ayanami", "Shinji Ikari", "Asuka Soryu" });
            }
        }

        [Test, Order(6)]
        public async Task SeaOfDirac()
        {
            var sync = 400;

            var result = await _repository.ReadEvaWithPilot("01")
                .Bind(async evaWithPilot => await _repository.UpdateEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id, sync));

            using (new AssertionScope())
            {
                result.IsSuccess.Should().BeTrue();
                result.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("01");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("01");
                result2.Value.Eva.Type.Should().Be("Test Type");
                result2.Value.Eva.Color.Should().Be("Purple");
                result2.Value.Pilot.FirstName.Should().Be("Shinji");
                result2.Value.Pilot.LastName.Should().Be("Ikari");
                result2.Value.Pilot.Title.Should().Be("Shinji You Idiot!");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(7)]
        public async Task TheFourthChildHasBeenFound()
        {
            var sync = 50;

            var result1 = await _repository.CreateEva("03", "Production Model", "Midnight Blue")
                .Bind(async eva => await _repository.CreatePilot("Toji", "Suzuhara", "Fourth Child", 14)
                    .Map(pilot => (eva, pilot)))
                .Bind(async evaPilot => await _repository.CreateEvaPilot(evaPilot.Item1.Id, evaPilot.Item2.Id, sync));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("03");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("03");
                result2.Value.Eva.Type.Should().Be("Production Model");
                result2.Value.Eva.Color.Should().Be("Midnight Blue");
                result2.Value.Pilot.FirstName.Should().Be("Toji");
                result2.Value.Pilot.LastName.Should().Be("Suzuhara");
                result2.Value.Pilot.Title.Should().Be("Fourth Child");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(8)]
        public async Task FatherWhatHaveYouDone()
        {
            var sync = 80;

            var result1 = await _repository.ReadEvaWithPilot("01")
                .Check(async evaWithPilot => await _repository.UpdateEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id, 0))
                .Check(async evaWithPilot => await _repository.DeleteEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id))
                .Bind(async evaWithPilot => await _repository.CreatePilot("Dummy", "Plug", "Dummy System", 0)
                    .Map(pilot => (evaWithPilot.Eva.Id, pilot.Id)))
                .Bind(async evaPilot => await _repository.CreateEvaPilot(evaPilot.Item1, evaPilot.Item2, sync))
                .Bind(async _ => await _repository.ReadPilot("Toji", "Suzuhara"))
                .Bind(async pilot => await _repository.DeletePilot(pilot.Id))
                .Bind(async _ => await _repository.ReadEva("03"))
                .Bind(async eva => await _repository.DeleteEva(eva.Id));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeEmpty();
            }

            var result2 = await _repository.ReadPilot("Toji", "Suzuhara");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeFalse();
                result2.IsFailure.Should().BeTrue();
            }

            var result3 = await _repository.ReadEva("03");

            using (new AssertionScope())
            {
                result3.IsSuccess.Should().BeFalse();
                result3.IsFailure.Should().BeTrue();
            }

            var result4 = await _repository.ReadEvaWithPilot("01");

            using (new AssertionScope())
            {
                result4.IsSuccess.Should().BeTrue();
                result4.Value.Should().NotBeNull();
                result4.Value.Eva.Unit.Should().Be("01");
                result4.Value.Eva.Type.Should().Be("Test Type");
                result4.Value.Eva.Color.Should().Be("Purple");
                result4.Value.Pilot.FirstName.Should().Be("Dummy");
                result4.Value.Pilot.LastName.Should().Be("Plug");
                result4.Value.Pilot.Title.Should().Be("Dummy System");
                result4.Value.Pilot.Age.Should().Be(0);
                result4.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(9)]
        public async Task IAmThePilotOfEvangelionUnitOne()
        {
            var sync = 100;

            var result1 = await _repository.ReadEvaWithPilot("01")
                .Bind(async evaWithPilot => await _repository.DeleteEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id))
                .Bind(async _ => await _repository.CreateEvaPilot("01", "Shinji", "Ikari", sync));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("01");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("01");
                result2.Value.Eva.Type.Should().Be("Test Type");
                result2.Value.Eva.Color.Should().Be("Purple");
                result2.Value.Pilot.FirstName.Should().Be("Shinji");
                result2.Value.Pilot.LastName.Should().Be("Ikari");
                result2.Value.Pilot.Title.Should().Be("Shinji You Idiot!");
                result2.Value.Pilot.Age.Should().Be(14);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(10)]
        public async Task HallelujahChorus()
        {
            var result1 = await _repository.ReadEvaWithPilot("02")
                .Check(async evaWithPilot => await _repository.UpdateEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id, 0))
                .Bind(async evaWithPilot => await _repository.DeleteEvaPilot(evaWithPilot.Eva.Id, evaWithPilot.Pilot.Id));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("02");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("02");
                result2.Value.Eva.Type.Should().Be("Combat Type");
                result2.Value.Eva.Color.Should().Be("Red");
                result2.Value.Pilot.Should().BeNull();
                result2.Value.SynchronizationRate.Should().BeNull();
            }
        }

        [Test, Order(11)]
        public async Task TheOneWhoIsCryingIsMe()
        {
            var result1 = await _repository.ReadEvaWithPilot("00")
                .Check(async evaWithPilot => await _repository.DeleteEva(evaWithPilot.Eva.Id))
                .Check(async evaWithPilot => await _repository.DeletePilot(evaWithPilot.Pilot.Id));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEva("00");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeFalse();
                result2.IsFailure.Should().BeTrue();
            }

            var result3 = await _repository.ReadPilot("Rei", "Ayanami");

            using (new AssertionScope())
            {
                result3.IsSuccess.Should().BeFalse();
                result3.IsFailure.Should().BeTrue();
            }
        }

        [Test, Order(12)]
        public async Task BecauseIThinkIAmProbablyTheThirdOne()
        {
            var result1 = await _repository.CreatePilot("Rei", "Ayanami", "Rei III", 14);

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadPilot("Rei", "Ayanami");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.FirstName.Should().Be("Rei");
                result2.Value.LastName.Should().Be("Ayanami");
                result2.Value.Title.Should().Be("Rei III");
                result2.Value.Age.Should().Be(14);
            }
        }

        [Test, Order(13)]
        public async Task TheGreatestAchievementOfTheLilimCulture()
        {
            var sync = 100;

            var result1 = await _repository.ReadEvaWithPilot("02")
                .Bind(async evaWithPilot => await _repository.CreatePilot("Kaworu", "Nagisa", "17th Angel", 15)
                    .Map(pilot => (evaWithPilot.Eva.Id, pilot.Id)))
                .Bind(async evaPilot => await _repository.CreateEvaPilot(evaPilot.Item1, evaPilot.Item2, sync));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeNull();
            }

            var result2 = await _repository.ReadEvaWithPilot("02");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("02");
                result2.Value.Eva.Type.Should().Be("Combat Type");
                result2.Value.Eva.Color.Should().Be("Red");
                result2.Value.Pilot.FirstName.Should().Be("Kaworu");
                result2.Value.Pilot.LastName.Should().Be("Nagisa");
                result2.Value.Pilot.Title.Should().Be("17th Angel");
                result2.Value.Pilot.Age.Should().Be(15);
                result2.Value.SynchronizationRate.Should().Be(sync);
            }
        }

        [Test, Order(14)]
        public async Task KaworuWhy()
        {
            var result1 = await _repository.ReadEvaWithPilot("02")
                .Bind(async evaWithPilot => await _repository.DeletePilot(evaWithPilot.Pilot.Id));

            using (new AssertionScope())
            {
                result1.IsSuccess.Should().BeTrue();
                result1.Value.Should().NotBeEmpty();
            }

            var result2 = await _repository.ReadEvaWithPilot("02");

            using (new AssertionScope())
            {
                result2.IsSuccess.Should().BeTrue();
                result2.Value.Should().NotBeNull();
                result2.Value.Eva.Unit.Should().Be("02");
                result2.Value.Eva.Type.Should().Be("Combat Type");
                result2.Value.Eva.Color.Should().Be("Red");
                result2.Value.Pilot.Should().BeNull();
                result2.Value.SynchronizationRate.Should().BeNull();
            }
        }

        [Test]
        public async Task AssertThat_ReadPilot_ById_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.ReadPilot(Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_ReadPilot_ByName_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.ReadPilot("Kensuke", "Aida");

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_UpdatePilot_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.UpdatePilot(Guid.NewGuid(), "Kensuke", "Aida", "I wish I could get inside that lovely cockpit just once!", 14);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_DeletePilot_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.DeletePilot(Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_CreateEva_ReturnsFailureWhenDuplicate()
        {
            var result = await _repository.CreateEva("01", "Test Type", "Purple");

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_ReadEva_ById_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.ReadEva(Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_ReadEva_ByUnit_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.ReadEva("Jet Alone");

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_UpdateEva_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.UpdateEva(Guid.NewGuid(), "Jet Alone", "Bipedal Robot", "White");

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_DeleteEva_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.DeleteEva(Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_CreateEvaPilot_ReturnsFailureWhenEvaNotFound()
        {
            var result = await _repository.CreateEvaPilot("101", "Rei", "Ayanami", 60);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_CreateEvaPilot_ReturnsFailureWhenPilotNotFound()
        {
            var result = await _repository.CreateEvaPilot("02", "Kensuke", "Aida", 60);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_CreateEvaPilot_ReturnsFailureWhenEvaAlreadyAssigned()
        {
            var result = await _repository.CreateEvaPilot("01", "Rei", "Ayanami", 60);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_CreateEvaPilot_ReturnsFailureWhenPilotAlreadyAssigned()
        {
            var result = await _repository.CreateEvaPilot("02", "Shinji", "Ikari", 60);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_ReadEvaPilot_ById_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.ReadEvaPilot(Guid.NewGuid(), Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_UpdateEvaPilot_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.UpdateEvaPilot(Guid.NewGuid(), Guid.NewGuid(), 60);

            result.IsFailure.Should().BeTrue();
        }

        [Test]
        public async Task AssertThat_DeleteEvaPilot_ReturnsFailureWhenNotFound()
        {
            var result = await _repository.DeleteEvaPilot(Guid.NewGuid(), Guid.NewGuid());

            result.IsFailure.Should().BeTrue();
        }
    }
}
