# CSharpFunctionalExtensionsTutorial

## What?

This repository is designed to help developers familiarize themselves with the CSharpFunctionalExtensions library.

## Why?

The CSharpFunctionalExtensions library is used extensively throughout the ZDiscovery backend, and can be tricky to get the hang of, as it asks developers working in a primarily object-oriented language to think in the context of a functional language. Trying to learn the ins and outs of this library in the middle of a PR review can make the entire process much more painful.

## How?

This repository contains two projects.

### CSFET_Repository

This project is home to the `ICSFETRepository` interface, which defines all of the CRUD methods the developer should implement.

The developer is expected to implement the `ICSFETRepository` methods as part of the `CSFETRepository` class, while relying heavily on the CSharpFunctionalExtensions library.

The `ExampleCSFETRepository` class includes examples of one possible implementation of the `ICSFETRepository` methods.

### CSFET_Tests

This project contains a single test suite used to run through the implementation of the `ICSFETRepository` methods. The tests here don't necessarily cover 100% of the `ICSFETRepository` methods, they're simply meant to walk through a hypothetical service layer that might make use of a repository layer that operates on and returns instances of the CSharpFunctionalExtensions `Result` type.

The test suite begins by creating an instance of the `ICSFETRepository` and initializing it with the "mock database" found in `Assets/json_repository.json`. By default the created instance will be a `CSFETRepository` class instance, but developers are free to change this so that they can run the tests using the already-complete implementations of the `ExampleCSFETRepository` class.

## Tips?

One useful trick I've found to help debug frustrating functional method combinations is to assign whichever functional methods successfully return to a variable, and then compare that value to whatever input is expected in the next functional method.

For example, if this code isn't working:

> var result = await \_repository.GetSomething()  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async something =&gt; await \_repository.GetSomethingElse(something))  
> &nbsp;&nbsp;&nbsp;&nbsp;.Map(async somethingElse =&gt;  
> &nbsp;&nbsp;&nbsp;&nbsp;{  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var x = await \_repository.DoAThing(somethingElse);  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return x.IsSuccess ? new Result.Success<Guid, string>(x.Id) : new Result.Failure<Guid, string>("Error");  
> &nbsp;&nbsp;&nbsp;&nbsp;})  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async yetAnotherThingId =&gt; await \_repository.GetYetAnotherThing(yetAnotherThingId));  

We can use Intellisense like this:

> var result = await \_repository.GetSomething()  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async something =&gt; await \_repository.GetSomethingElse(something))  
> &nbsp;&nbsp;&nbsp;&nbsp;.Map(async somethingElse =&gt;  
> &nbsp;&nbsp;&nbsp;&nbsp;{  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var x = await \_repository.DoAThing(somethingElse);  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return x.IsSuccess ? new Result.Success<Guid, string>(x.Id) : new Result.Failure<Guid, string>("Error");  
> &nbsp;&nbsp;&nbsp;&nbsp;});  
> result.Bind(async yetAnotherThingId =&gt; \_repository.GetYetAnotherThing(yetAnotherThingId));  

And we can see that the final return value of that call to `.Map()` will return a `Result<Result<Guid, string>>`, which the subsequent call to `.Bind()` will have a harder time operating on. Since `.Map()` wraps a return value in a successful `Result`, and `.Bind()` simply passes through a `Result`, one possible solution would be code that looks like this:

> var result = await \_repository.GetSomething()  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async something =&gt; await \_repository.GetSomethingElse(something))  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async somethingElse =&gt; await \_repository.DoAThing(somethingElse))  
> &nbsp;&nbsp;&nbsp;&nbsp;.Bind(async yetAnotherThing =&gt; await \_repository.GetYetAnotherThing(yetAnotherThing.Id));  